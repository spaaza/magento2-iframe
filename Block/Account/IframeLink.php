<?php

namespace Spaaza\Iframe\Block\Account;

class IframeLink extends \Magento\Customer\Block\Account\SortLink
{
    /**
     * Conditionally show html
     *
     * @return string
     */
    protected function _toHtml()
    {
        $show = false;
        if ($viewModel = $this->getNavigationViewModel()) {
            $show = $viewModel->showNavigationLink();
        }
        if ($show) {
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * return \Spaaza\Iframe\ViewModel\Navigation
     */
    protected function getNavigationViewModel(): ?\Spaaza\Iframe\ViewModel\Navigation
    {
        return $this->getData('spaaza_view_model');
    }
}
