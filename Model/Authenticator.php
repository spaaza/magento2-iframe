<?php

namespace Spaaza\Iframe\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;

class Authenticator
{
    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    protected $requestFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    protected $requestQueue;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->requestFactory = $requestFactory;
        $this->requestQueue = $requestQueue;
        $this->storeManager = $storeManager;
    }

    /**
     * Start a new user session through the Spaaza API
     *
     * @param CustomerInterface $customer
     * @return array|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function fetchUserSession(CustomerInterface $customer): ?array
    {
        $spaazaUserId = $customer->getExtensionAttributes()->getSpaazaData()->getUserId();
        if (!$spaazaUserId) {
            return null;
        }

        $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())
            ->getDefaultStore()
            ->getId();

        $request = $this->requestFactory->create()
            ->setStoreId($storeId)
            ->setPath('auth/session')
            ->setMethod(RequestInterface::METHOD_POST)
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true])
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setPayload(
                [
                    'user_id' => $customer->getExtensionAttributes()->getSpaazaData()->getUserId()
                ]
            );
        return $this->requestQueue->sendSynchronousRequest($request);
    }
}
