<?php

namespace Spaaza\Iframe\Model;

use Magento\Store\Model\ScopeInterface;

class Config
{
    const XML_PATH_ACCOUNT_LINK_SHOW = 'spaaza_loyalty/iframe/account_link_show';
    const XML_PATH_ACCOUNT_LINK_LABEL = 'spaaza_loyalty/iframe/account_link_label';
    const XML_PATH_PAGE_TITLE = 'spaaza_loyalty/iframe/page_title';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Show a link in the customer account navigation?
     *
     * @return bool
     */
    public function showLinkInCustomerAccount(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ACCOUNT_LINK_SHOW,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the configured label for customer account link
     *
     * @return string
     */
    public function getCustomerAccountLinkLabel(): string
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_ACCOUNT_LINK_LABEL,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the configured page title
     *
     * @return string
     */
    public function getPageTitle(): string
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_PAGE_TITLE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the configured Spaaza hostname
     *
     * @return string
     */
    public function getIframeHostname(): string
    {
        return (string)$this->scopeConfig->getValue(
            \Spaaza\Loyalty\Model\Client\Config::XML_PATH_SPAAZA_HOSTNAME,
            ScopeInterface::SCOPE_STORE
        );
    }
}
