<?php

namespace Spaaza\Iframe\Model;

class SpaazaSession
{
    const SESSION_USER_ID = 'session_user_id';
    const SESSION_KEY = 'session_key';
    const SESSION_EXPIRES = 'session_expires';

    const CUSTOMER_SESSION_KEY = 'spaaza_iframe_auth';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var Authenticator
     */
    protected $authenticator;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Has an error occurred during initializing the Spaaza session?
     *
     * @var bool
     */
    protected $hasError = false;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        Authenticator $authenticator,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->customerSession = $customerSession;
        $this->authenticator = $authenticator;
        $this->logger = $logger;
    }

    /**
     * Get session user id
     *
     * @return int|null
     */
    public function getSessionUserId(): ?int
    {
        $data = $this->getSessionData();
        return !empty($data[self::SESSION_USER_ID])
            ? (int)$data[self::SESSION_USER_ID]
            : null;
    }

    /**
     * Het session key
     *
     * @return string|null
     */
    public function getSessionKey(): ?string
    {
        $data = $this->getSessionData();
        return !empty($data[self::SESSION_KEY])
            ? (string)$data[self::SESSION_KEY]
            : null;
    }

    /**
     * Get session data from customer session OR fetch new
     *
     * @return array|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getSessionData(): ?array
    {
        if ($this->hasError) {
            // We already tried during this request
            return null;
        }
        if ($data = $this->customerSession->getData(self::CUSTOMER_SESSION_KEY)) {
            return $data;
        } else {
            // Try to fetch a session key
            try {
                $sessionData = $this->authenticator->fetchUserSession($this->customerSession->getCustomerData());
                if (!empty($sessionData['session_info'][self::SESSION_KEY])
                    && !empty($sessionData['session_info'][self::SESSION_USER_ID])
                ) {
                    $this->customerSession->setData(self::CUSTOMER_SESSION_KEY, $sessionData['session_info']);
                    return $sessionData['session_info'];
                }
            } catch (\Exception $exception) {
                $this->logger->error($exception);
            }
        }

        $this->hasError = true;
        return null;
    }
}
