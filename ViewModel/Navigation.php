<?php

namespace Spaaza\Iframe\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Navigation implements ArgumentInterface
{
    /**
     * @var \Spaaza\Iframe\Model\Config
     */
    protected $config;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    public function __construct(
        \Spaaza\Iframe\Model\Config $config,
        \Magento\Customer\Model\Session $session
    ) {
        $this->config = $config;
        $this->session = $session;
    }

    /**
     * Show the Iframe link in customer navigation?
     *
     * @return bool
     */
    public function showNavigationLink(): bool
    {
        if (!$this->config->showLinkInCustomerAccount()) {
            return false;
        }
        if ($this->session->isLoggedIn() && $customer = $this->session->getCustomerData()) {
            if (empty($customer->getExtensionAttributes()->getSpaazaData()->getUserId())) {
                return false;
            }
        }
        return true;
    }
}
