<?php

namespace Spaaza\Iframe\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Iframe implements ArgumentInterface
{
    /**
     * @var \Spaaza\Iframe\Model\SpaazaSession
     */
    protected $spaazaSession;

    /**
     * @var \Spaaza\Iframe\Model\Config
     */
    protected $config;

    public function __construct(
        \Spaaza\Iframe\Model\SpaazaSession $spaazaSession,
        \Spaaza\Iframe\Model\Config $config
    ) {
        $this->spaazaSession = $spaazaSession;
        $this->config = $config;
    }

    /**
     * Get the Spaaza session key
     *
     * @return string
     */
    public function getSessionKey(): string
    {
        return $this->spaazaSession->getSessionKey();
    }

    /**
     * Get the Spaaza session user id
     *
     * @return string
     */
    public function getUserId(): string
    {
        return $this->spaazaSession->getSessionUserId();
    }

    /**
     * Get the URL parameters necessary for building the iframe's src attribute
     *
     * @return string[]
     */
    public function getIframeParameters(): array
    {
        return [
            'user_id' => $this->getUserId(),
            'session_key' => $this->getSessionKey()
        ];
    }

    /**
     * Get the URL to use in the iframe's src attribute
     * @return string
     */
    public function getIframeSrc(): string
    {
        return 'https://'
            . $this->config->getIframeHostname()
            . '?'
            . http_build_query($this->getIframeParameters());
    }
}
